package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerPortStr();

    @NotNull
    String getServerHost();
}
