package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.api.service.IUserOwnerService;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel, R extends IUserOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnerService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return repository.findAll(userId);
    }


    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return repository.findAll();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(userId, model);
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeOneByIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

}
