package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

}
