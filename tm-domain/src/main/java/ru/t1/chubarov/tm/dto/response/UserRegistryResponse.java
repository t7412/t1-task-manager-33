package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.User;

@Getter
@Setter
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
