package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    @Nullable
    private Throwable throwable;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    public AbstractUserResponse(@Nullable final Throwable throwable) {
        this.throwable = throwable;
    }

}
