package ru.t1.chubarov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.enumerated.TaskSort;

@Getter
@Setter
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public TaskListRequest(@Nullable final TaskSort sort) {
        this.sort = sort;
    }

}
