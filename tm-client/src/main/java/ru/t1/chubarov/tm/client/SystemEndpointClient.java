package ru.t1.chubarov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chubarov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chubarov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.chubarov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.chubarov.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    public SystemEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

}
