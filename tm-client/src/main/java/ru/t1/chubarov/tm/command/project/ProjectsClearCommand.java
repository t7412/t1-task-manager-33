package ru.t1.chubarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.ProjectClearRequest;
import ru.t1.chubarov.tm.exception.AbstractException;

public final class ProjectsClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAR]");

        @NotNull final ProjectClearRequest request = new ProjectClearRequest();
        getProjectEndpoint().clearProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all project.";
    }

}
