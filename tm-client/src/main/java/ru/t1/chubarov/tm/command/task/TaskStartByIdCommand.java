package ru.t1.chubarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final Status status = Status.toStatus("IN_PROGRESS");
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setId(id);
        request.setStatus(status);
        getTaskEndpoint().changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task status to [In progress] by id.";
    }

}
