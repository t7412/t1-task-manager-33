package ru.t1.chubarov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    void connect() throws IOException;

    void disconnect() throws IOException;

    Socket getSocket();

    void setSocket(@Nullable Socket socket);

}
