package ru.t1.chubarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

public interface IUserEndpointClient {
    @NotNull
    @SneakyThrows
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    @SneakyThrows
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    @SneakyThrows
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    @SneakyThrows
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    @SneakyThrows
    UserUpdateProfileResponse updateProfileUser(@NotNull UserUpdateProfileRequest request);

    @NotNull
    @SneakyThrows
    UserProfileResponse viewProfileUser(@NotNull UserProfileRequest request);

    @NotNull
    @SneakyThrows
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);
}
