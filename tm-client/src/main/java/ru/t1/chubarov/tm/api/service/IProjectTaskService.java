package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.chubarov.tm.exception.field.TaskIdEmptyException;

public interface IProjectTaskService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) throws AbstractException;

    void removeProjectById(@NotNull String userId, @NotNull String projectId) throws AbstractException;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws AbstractException;

}
